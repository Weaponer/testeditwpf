﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Win32;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

namespace TextEditorWPF.Files
{
    //Статичный класс для загрузки сохранения файлов.
    public static class FileSystem
    {

        public static void SaveTextFile(TextFile file)//Сохранение текста
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = ("Text File (*." + TextFile.Type + ")|*." + TextFile.Type);
            if (saveFileDialog.ShowDialog() == true)
            {
                BinaryFormatter binary = new BinaryFormatter();
                FileStream stream = File.Open(saveFileDialog.FileName, FileMode.OpenOrCreate);
                binary.Serialize(stream, file);
                stream.Close();
            }
        }

        public static TextFile[] LoadTextFiles()//Загрузка списка файлов с текстами (В окне загрузки с шифтом выбрать файлы)
        {
            TextFile[] texts = new TextFile[0];

            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = ("Text File (*." + TextFile.Type + ")|*." + TextFile.Type);
            openFileDialog.Multiselect = true;
            if (openFileDialog.ShowDialog() == true)
            {
                string[] paths = openFileDialog.FileNames;
                texts = new TextFile[paths.Length];
                for (int i = 0; i < paths.Length; i++)
                {
                    BinaryFormatter binary = new BinaryFormatter();
                    FileStream file = File.Open(paths[i], FileMode.Open);
                    texts[i] = new TextFile();
                    texts[i] = (TextFile)binary.Deserialize(file);
                    string name = Path.GetFileName(paths[i]);
                    name = name.Remove(name.IndexOf("." + TextFile.Type));
                    texts[i].SetName(name);
                    file.Close();
                }
            }
            return texts;
        }

        public static void SaveTestFile(TestFile file)//Сохранение теста
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = ("Text File (*." + TestFile.Type + ")|*." + TestFile.Type);
            if (saveFileDialog.ShowDialog() == true)
            {
                BinaryFormatter binary = new BinaryFormatter();
                FileStream stream = File.Open(saveFileDialog.FileName, FileMode.OpenOrCreate);
                binary.Serialize(stream, file);
                stream.Close();
            }
        }

        public static TestFile LoadTestFiles()//Загрузка теста
        {
            TestFile texts =null;

            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = ("Text File (*." + TestFile.Type + ")|*." + TestFile.Type);
            if (openFileDialog.ShowDialog() == true)
            {
                string paths = openFileDialog.FileName;

                BinaryFormatter binary = new BinaryFormatter();
                FileStream file = File.Open(paths, FileMode.Open);
                texts = (TestFile)binary.Deserialize(file);

                string name = Path.GetFileName(paths);
                name = name.Remove(name.IndexOf("." + TestFile.Type));
                texts.SetName(name);
                file.Close();
            }
            return texts;
        }

        public static void SaveParams(OptionFile option)// Сохарение параметров настройки
        {
            if (!Directory.Exists(OptionFile.Path + @"\" + OptionFile.NameDirectory))
            {
                Directory.CreateDirectory(OptionFile.Path + @"\" + OptionFile.NameDirectory);
            }

            BinaryFormatter binary = new BinaryFormatter();
            FileStream stream = File.Open(OptionFile.Path + @"\" + OptionFile.NameDirectory + @"\" + OptionFile.NameFile + "." + OptionFile.Type, FileMode.OpenOrCreate);
            binary.Serialize(stream, option);
            stream.Close();
        }

        public static OptionFile LoadParams()//Загрузка файла с параметрами настройки
        {
            OptionFile optionFile;
            if (Directory.Exists(OptionFile.Path + @"\" + OptionFile.NameDirectory))
            {
                BinaryFormatter binary = new BinaryFormatter();
                FileStream stream = File.Open(OptionFile.Path + @"\" + OptionFile.NameDirectory + @"\" + OptionFile.NameFile + "." + OptionFile.Type, FileMode.Open);
                optionFile = (OptionFile)binary.Deserialize(stream);
                stream.Close();
            }
            else
            {
                optionFile = new OptionFile();
                optionFile.BackgroundBrightness = 0f;
                optionFile.ButtonBrightness = 10f;
                optionFile.SizeFont = 5f;
                optionFile.TextBrightness = 10f;
                Directory.CreateDirectory(OptionFile.Path + @"\" + OptionFile.NameDirectory);
            }
            return optionFile;
        }
    }
}
