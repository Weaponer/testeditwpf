﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TextEditorWPF.Files
{
    [Serializable]
    public class OptionFile//Файл для хранения настроек
    {
        public static readonly string Type = "opt";

        public static readonly string NameDirectory = "TextEditor";

        public static readonly string Path = @"C:\Users\Oleg\Documents";

        public static readonly string NameFile = "OPT";

        public float BackgroundBrightness;

        public float TextBrightness;

        public float ButtonBrightness;

        public float SizeFont;
    }
}
