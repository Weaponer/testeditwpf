﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TextEditorWPF
{
    [Serializable]
    public class TestFile// Файл для хранения теста
    {
        public static readonly string Type = "tes";

        public string Name { get; private set; }

        TextFile[] texts;

        public void SetName(string name)
        {
            Name = name;
        }

        public void SetTexts(TextFile[] texts)
        {
            this.texts = texts;
        }

        public TextFile[] GetTexts()
        {
            return texts;
        }
    }
}
