﻿using System;
using System.Collections.Generic;
using System.Text;
using TextEditorWPF.Words;

namespace TextEditorWPF
{
    [Serializable]
    public class TextFile // Файл для хранения текста
    {
        public static readonly string Type = "tex";

        public string Name { get; private set; }

        private Word[] baseText;
        private Word[] testText;

        public void WriteWords(Word[] baseText, Word[] testText)
        {
            this.baseText = baseText;
            this.testText = testText;
        }

        public void SetName(string name)
        {
            Name = name;
        }

        public Word[] GetBaseText()
        {
            return baseText;
        }

        public Word[] GetAlteredText()
        {
            return testText;
        }
    }
}
