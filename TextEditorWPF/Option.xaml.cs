﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using TextEditorWPF.Panels;

namespace TextEditorWPF
{
    /// <summary>
    /// Логика взаимодействия для Option.xaml
    /// </summary>
    public partial class Option : Window //Окно с настройками. Нужно только для создания панели и взятия елементов интерфейса.
    {
        public Option()
        {
            InitializeComponent();
        }

        public Button GetClose()
        {
            return CloseOption;
        }

        public Slider GetBackgroundBrightness()
        {
            return BackgroundBrightness;
        }

        public Slider GetTextBrightness()
        {
            return TextBrightness;
        }

        public Slider GetButtonBrightness()
        {
            return ButtonBrightness;
        }

        public Slider GetSizeFont()
        {
            return SizeFont;
        }

        public ResourceDictionary GetResource()
        {
            return this.Resources;
        }
    }
}
