﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TextEditorWPF.Panels;

namespace TextEditorWPF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {

            InitializeComponent();
            InitPanels();
            InitOption();

            Closed += CloseBase;
        }

        private void InitPanels()//Инициализация программы и создание всех основных объектов.
        {

            BaseMenuPanel baseMenu = new BaseMenuPanel();
            baseMenu.SetGrid(BaseMenu);
            MenuCreateTestPanel menuCreate = new MenuCreateTestPanel();
            menuCreate.SetGrid(CreateNewTestMenu);
            CreateTextPanel createTextPanel = new CreateTextPanel();
            createTextPanel.SetGrid(TextCreatePanel);
            EditTextPanel editTextPanel = new EditTextPanel();
            editTextPanel.SetGrid(TextEditPanel);
            CreateTestPanel createTest = new CreateTestPanel();
            createTest.SetGrid(TestCreatePanel);
            PassTestPanel passTest = new PassTestPanel();
            passTest.SetGrid(PassingTestPanel);


            PanelControll.SetPanels(baseMenu, menuCreate, createTextPanel, editTextPanel, createTest, passTest);//Установка в главный контролер и библиотеки созданные панели.

            baseMenu.SetButtons(CreateNewTest, PassTest);
            menuCreate.SetButtons(CreateText, EditText, UniteTexts, BackInBaseMenu);
            createTextPanel.SetElements(BackTextCreatePanel, ReadyTextCreatePanel, SaveTextCreatePanel, TextBoxCreatePanelText, PromptTextCreatePanel);
            editTextPanel.SetElement(BackTextEditPanel, OldTextBoxEditPanelText, NewTextBoxEditPanelText, LoadTextEditPanel, SaveTextEditPanel);
            createTest.SetElement(BackTestCreatePanel, SaveTestCreatePanel, LoadTestCreatePanel, FilesTestCreatePanel);
            passTest.SetElements(ClosePassingTestPanel, NextPassingTestPanel, BackPassingTestPanel, LoadPassingTestPanel, BoxTextPassingTestPanel, ListIconPassiongTestPanel, NameTextPassingTestPanel, NameTestPassingTestPanel);

            PanelControll.OpenPanel(PanelControll.BaseMenu);
        }

        private void InitOption()//Инициализация панели настроек
        {
            OptionPanel optionPanel = new OptionPanel();
            optionPanel.SetElement(OpenOption, Resources);
            PanelControll.SetOption(optionPanel);
            optionPanel.Init();
        }

        private void CloseBase(object sende, EventArgs e)//Событие выхода их программы.
        {
            PanelControll.Option.Destroy();
            Application.Current.Shutdown();
        }
    }
}
