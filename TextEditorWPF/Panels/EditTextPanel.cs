﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using TextEditorWPF.Files;
using TextEditorWPF.Words;

namespace TextEditorWPF.Panels
{
    public class EditTextPanel : Panel//Панель для редактирования текста
    {
        private Button back;

        private TextBox oldTextField;

        private TextBox newTextField;

        private Button load;

        private Button save;

        TextFile textFile;

        string saveNewText;


        public void SetElement(Button back, TextBox oldTextField, TextBox newTextField, Button load, Button save)
        {
            this.back = back;
            this.oldTextField = oldTextField;
            this.newTextField = newTextField;
            this.save = save;
            this.load = load;
            SetEvent();
        }

        private void SetEvent()
        {
            back.Click += BackToCreateMenu;
            load.Click += LoadFile;
            newTextField.TextChanged += EditText;
            save.Click += SaveFile;
        }

        private void BackToCreateMenu(object sender, RoutedEventArgs e)//Загрузка в панель с начальным меню.
        {
            PanelControll.OpenPanel(PanelControll.MenuCreateTest);
        }

        private void LoadFile(object sender, RoutedEventArgs e)
        {
            Reset();
            LoadAndSetOnEditText();
        }

        private void SaveFile(object sender, RoutedEventArgs e)
        {
            Save();
        }

        private void EditText(object sender, RoutedEventArgs e)
        {
            EditText();
        }

        protected override void CallClose()
        {
            Reset();
        }

        private void Reset()//Обнуление панели
        {
            if (textFile != null)
            {
                textFile = null;
                oldTextField.Text = "";
                newTextField.Text = "";
                saveNewText = "";
            }
        }

        private void LoadAndSetOnEditText()//Загрузка нового файла с текстом для редактирования.
        {
            TextFile[] text = Files.FileSystem.LoadTextFiles();
            if (text.Length > 0)
            {
                SetOnEditText(text[0]);
            }
        }

        private void Save()//Сохранение изменений
        {
            TextFile text = new TextFile();
            text.WriteWords(textFile.GetBaseText(), TextInformation.GetArrayWordsFromText(newTextField.Text));
            FileSystem.SaveTextFile(text);
        }

        private void EditText()//Пользователь ввел новые данные в панель.
        {
            if (textFile != null)
            {
                Word[] newWord = TextInformation.GetArrayWordsFromText(newTextField.Text);
                bool IsCoincidece = true;
                if (newWord.Length != textFile.GetBaseText().Length)
                {
                    newTextField.Text = saveNewText;
                    return;
                }
                else
                {
                    for (int i = 0; i < newWord.Length; i++)
                    {
                        if (newWord[i].Letters != textFile.GetBaseText()[i].Letters)
                        {
                            IsCoincidece = false;
                        }
                    }
                }

                if (IsCoincidece)
                {
                    newTextField.Text = saveNewText;
                    return;
                }
                else
                {
                    saveNewText = new string(newTextField.Text);
                }
            }
        }

        private void SetOnEditText(TextFile text)//Загрузка в панель нового текста.
        {
            textFile = text;
            oldTextField.Text = TextInformation.GetText(textFile.GetBaseText());
            saveNewText = TextInformation.GetText(textFile.GetAlteredText());
            newTextField.Text = saveNewText;

        }
    }
}
