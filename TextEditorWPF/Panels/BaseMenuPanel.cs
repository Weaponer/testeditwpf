﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;

namespace TextEditorWPF.Panels
{
    public class BaseMenuPanel : Panel
    {
        private Button createTest;

        private Button passingTest;
        public void SetButtons(Button createTest, Button passingTest)//При создания каждой панели есть подобный метод который отправляет панели список елементов ие интерфейса.
        {
            this.createTest = createTest;
            this.passingTest = passingTest;
            SetEvents();
        }

        private void SetEvents()//Подключает события полученных елементов к внутренним методам.
        {
            createTest.Click += OpenCreateTestPanel;
            passingTest.Click += OpenPassTestPanel;
        }

        private void OpenCreateTestPanel(object sender, RoutedEventArgs e)//Событие перехода в панель с меню и для создания теста.
        {
            PanelControll.OpenPanel(PanelControll.MenuCreateTest);
        }

        private void OpenPassTestPanel(object sender, RoutedEventArgs e)//Событие для перехода в панель выполнения теста.
        {
            PanelControll.OpenPanel(PanelControll.PassTest);
        }
    }
}
