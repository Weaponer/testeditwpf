﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using TextEditorWPF.Files;
using TextEditorWPF;
using System.Windows.Media;

namespace TextEditorWPF.Panels
{
    public class OptionPanel
    {
        Button open;

        Option option;

        OptionFile optionFile;

        ResourceDictionary windowBase;


        public void SetElement(Button open, ResourceDictionary baseWind)
        {
            this.open = open;

            windowBase = baseWind;
        }
        public void Init()//Инициализация панели настроек
        {
            open.Click += OpenClick;


            InitOption();

        }

        private void InitOption()//Инициализация нового окна настроек
        {
            option = new Option();
            option.GetClose().Click += CloseClick;
            option.Closed += CloseWind;

            LoadParams();
            SetParamsSlider();
            SetParams();

            option.GetBackgroundBrightness().ValueChanged += UpdateParams;
            option.GetTextBrightness().ValueChanged += UpdateParams;
            option.GetButtonBrightness().ValueChanged += UpdateParams;
            option.GetSizeFont().ValueChanged += UpdateParams;
        }

        private void OpenClick(object sender, RoutedEventArgs e)//Событие открытия.
        {
            if (option != null)
            {
                option.Show();
            }
            else
            {
                InitOption();
                option.Show();
            }
        }

        private void CloseClick(object sender, RoutedEventArgs e)//Событие закрытия
        {
            option.Close();
            SaveParams();
            option = null;
        }

        private void CloseWind(object sender, EventArgs e)//Событие закрытия
        {
            SaveParams();
            option = null;
        }



        private void UpdateParams(object sender, RoutedEventArgs e)
        {
            SetParams();
        }

        private void SaveParams()
        {
            Files.FileSystem.SaveParams(optionFile);
        }

        private void LoadParams()
        {
            optionFile = Files.FileSystem.LoadParams();
        }

        private void SetParamsSlider()//Утсановка загруженых параметров в меню настроек.
        {
            option.GetBackgroundBrightness().Value = optionFile.BackgroundBrightness;
            option.GetButtonBrightness().Value = optionFile.ButtonBrightness;
            option.GetSizeFont().Value = optionFile.SizeFont;
            option.GetTextBrightness().Value = optionFile.TextBrightness;
        }

        private void SetParams()//Установка параметров в программу и файл сохранения.
        {
            optionFile.BackgroundBrightness = (float)option.GetBackgroundBrightness().Value;
            optionFile.ButtonBrightness = (float)option.GetButtonBrightness().Value;
            optionFile.SizeFont = (float)option.GetSizeFont().Value;
            optionFile.TextBrightness = (float)option.GetTextBrightness().Value;

            byte color = (byte)((optionFile.BackgroundBrightness / 10f) * 255f);
            option.GetResource()["BaseBackground"] = new SolidColorBrush(Color.FromRgb(color, color, color));
            windowBase["BaseBackground"] = new SolidColorBrush(Color.FromRgb(color, color, color));


            byte colorButton = (byte)((optionFile.ButtonBrightness / 10f) * 255f);
            option.GetResource()["ColorButton"] = new SolidColorBrush(Color.FromRgb(colorButton, colorButton, colorButton));
            windowBase["ColorButton"] = new SolidColorBrush(Color.FromRgb(colorButton, colorButton, colorButton));

            byte colorText = (byte)((optionFile.TextBrightness / 10f) * 255f);
            option.GetResource()["ColorText"] = new SolidColorBrush(Color.FromRgb(colorText, colorText, colorText));
            windowBase["ColorText"] = new SolidColorBrush(Color.FromRgb(colorText, colorText, colorText));

            double sizeText = (optionFile.SizeFont / 10f) * 15f + 10;
            option.GetResource()["FontSize"] = sizeText;
            windowBase["FontSize"] = sizeText;
        }

        public void Destroy()//Уничтожение окна.
        {
            if (option != null)
            {
                option.Close();
            }
        }
    }
}
