﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using TextEditorWPF.Files;
using TextEditorWPF.Panels;
using TextEditorWPF.Words;
using System.Windows;
using System.Windows.Controls;
using System.Diagnostics;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Numerics;
using System.IO;
using System.Threading.Tasks;

namespace TextEditorWPF.Panels
{
    public class PassTestPanel : Panel
    {
        Button close;
        Button back;
        Button next;
        Button load;
        TextBox boxTextPassing;
        StackPanel listIcon;
        TextBlock nameTextPass;
        TextBlock nameTestPass;

        TestFile test;

        PointRender pointRender;

        int numText;

        bool isChangedText;

        bool passTest;

        string saveText;

        //При создания каждой панели есть подобный метод который отправляет панели список елементов ие интерфейса.
        public void SetElements(Button close, Button next, Button back, Button load, TextBox boxTextPassing, StackPanel listIcon, TextBlock nameTextPass, TextBlock nameTestPass)
        {
            this.close = close;
            this.load = load;
            this.next = next;
            this.back = back;
            this.boxTextPassing = boxTextPassing;
            this.listIcon = listIcon;
            this.nameTestPass = nameTestPass;
            this.nameTextPass = nameTextPass;

            //Инициализация окна

            pointRender = new PointRender(listIcon);//Создания обьекта для контроля рендера и анимации иконок

            SetEvent();
            CorrectMoveButton();
        }

        protected override void CallClose()//Переопределение метода который вызываеться при закрытии панели
        {
            Reset();
            load.Visibility = Visibility.Visible;
        }

        private void Reset()//Обнуление окна
        {
            test = null;
            numText = 0;
            passTest = false;
            isChangedText = true;
            boxTextPassing.Text = "";
            saveText = "";
            isChangedText = false;
            nameTestPass.Text = "Тест: ";
            nameTextPass.Text = "Текст: ";
            pointRender.ResetTo(0);

            CorrectMoveButton();
        }

        private void SetEvent()//Подключает события полученных елементов к внутренним методам.
        {
            close.Click += BackToMenu;
            load.Click += LoadClickTest;
            next.Click += NextClick;
            back.Click += BackTextClick;
            boxTextPassing.TextChanged += ChangedTextBox;
        }

        private void NextClick(object sender, RoutedEventArgs e)//Пользователь нажал на кнопку далее
        {
            if (numText < test.GetTexts().Length - 1)
            {
                numText++;
                LoadText(numText, numText - 1);
                CorrectMoveButton();
            }
        }

        private void BackTextClick(object sender, RoutedEventArgs e)//Пользователь нажал на кнопку назад
        {
            if (numText < test.GetTexts().Length)
            {
                numText--;
                LoadText(numText, numText + 1);
                CorrectMoveButton();
            }
        }

        private void CorrectMoveButton()//Определяет на каком тексте сейчас пользователь по списку и куда может двигаться.
        {


            if (test != null && numText > 0 && test.GetTexts().Length != 1)
            {
                back.Visibility = Visibility.Visible;
            }
            else
            {
                back.Visibility = Visibility.Collapsed;
            }

            if (test != null && numText < test.GetTexts().Length - 1 && test.GetTexts().Length != 1)
            {
                next.Visibility = Visibility.Visible;
            }
            else
            {
                next.Visibility = Visibility.Collapsed;
            }

        }

        private void LoadClickTest(object sender, RoutedEventArgs e)//Загрузить тест
        {
            Load();
        }

        private void BackToMenu(object sender, RoutedEventArgs e)//Вернуться в меню
        {
            PanelControll.OpenPanel(PanelControll.BaseMenu);
        }

        private void ChangedTextBox(object sender, RoutedEventArgs e)//Пользователь внес изменение в панель для ввода ответов
        {
            if (!isChangedText)//Проверка на то что данные вносит программа, а не пользователь. Если вносит программа тогда следуюший код не выполняеться.
            {
                if (!passTest)//Если пользователь не загрузил тест на прохождение то включаеться авто возврат к предыдущему состоянию.
                {
                    isChangedText = true;
                    boxTextPassing.Text = saveText;
                    isChangedText = false;
                }
                else
                { 
                    //Далее идет проверка на то что пользователь не изменил количиство слов и если да то возвращение до изменения текста или же сохранение изменений и обновление анимации иконок
                    Word[] words = TextInformation.GetArrayWordsFromText(boxTextPassing.Text);
                    if (words.Length != test.GetTexts()[numText].GetBaseText().Length)
                    {
                        isChangedText = true;
                        boxTextPassing.Text = saveText;
                        isChangedText = false;
                        return;
                    }

                    saveText = boxTextPassing.Text;
                    pointRender.SetParams(GetCoincidences());//Получение списка правельных и не правельных ответов и запись в обработчик анимаций иконок.

                }
            }
        }

        private void Load()//Загрузка теста из файлов
        {
            TestFile testFile = FileSystem.LoadTestFiles();
            if (testFile != null)
            {
                load.Visibility = Visibility.Collapsed;
                Reset();
                test = testFile;
                StartPassTest();
                CorrectMoveButton();
            }
        }

        private void StartPassTest()//Начать прохождение теста
        {
            nameTestPass.Text = "Тест: " + test.Name;
            passTest = true;
            LoadText(0, 0);
        }

        private bool[] GetCoincidences()//Получает список правельных и не правельных слов.
        {
            bool[] c = new bool[test.GetTexts()[numText].GetBaseText().Length];
            Word[] baseText = test.GetTexts()[numText].GetBaseText();
            Word[] thisText = TextInformation.GetArrayWordsFromText(boxTextPassing.Text);
            for (int i = 0; i < baseText.Length; i++)
            {
                var a = baseText[i].Letters;
                var b = thisText[i].Letters;
                c[i] = a == b;
            }
            return c;
        }

        private void LoadText(int i, int oldText)// Загрузить текст из теста в панель. А так же сохранить изменение в предыдущий текст.
        {
            nameTextPass.Text = "Текст: " + test.GetTexts()[i].Name;

            if (i != oldText)
            {
                test.GetTexts()[oldText].WriteWords(test.GetTexts()[oldText].GetBaseText(), TextInformation.GetArrayWordsFromText(saveText));
            }

            Word[] words = test.GetTexts()[i].GetAlteredText();
            pointRender.ResetTo(words.Length);
            TextInformation.TabWords(words);
            saveText = TextInformation.GetText(words);
            isChangedText = true;
            boxTextPassing.Text = saveText;
            isChangedText = false;
            pointRender.SetParams(GetCoincidences());
        }


        // Этот класс обработки анимации иконок выполненых слов.
        //Данных класс запускает отдельный поток от программы для выполения плавной анимации. Обновление картинки происходит минимум за 10 милисекунд.
        private class PointRender
        {
            List<Point> points = new List<Point>();//Список иконок с анимациями для хранения

            StackPanel list;

            BitmapSource trueImage;
            BitmapSource falseImage;

            BitmapSource greenFon;
            BitmapSource redFon;

            bool updateList;//Это предохранитель. Когда он равен True другой поток для рендера останавливаеться. Это означает что количиство иконок и анимаций для них изменяеться.
            public PointRender(StackPanel list)//Инициализаия
            {
                this.list = list;
                LoadBitmaps();

                //Асинхронный запуск так называймого "Отдельного потока" для анимации.
                AsyncUpdate();
            }

            private void LoadBitmaps()//Загрузка в память всех необходимых картинок и их модификация
            {

                using (MemoryStream stream = new MemoryStream(Properties.Resources.ToggleFalse))//Подключение потока для работы с памятью. И загрузка из ресурсов программы png картинки.
                {
                    BitmapImage image = new BitmapImage();
                    image.BeginInit();//начало перезаписи объекта для хранения картинки
                    image.CacheOption = BitmapCacheOption.OnLoad;//режим записи
                    image.StreamSource = stream;//установка потока для записи
                    image.EndInit();//прекращение перезаписи
                    falseImage = image;
                }

                using (MemoryStream stream = new MemoryStream(Properties.Resources.ToggleTrue))
                {
                    BitmapImage image = new BitmapImage();
                    image.BeginInit();
                    image.CacheOption = BitmapCacheOption.OnLoad;
                    image.StreamSource = stream;
                    image.EndInit();
                    trueImage = image;
                }

                BitmapSource bitFon = null;

                using (MemoryStream stream = new MemoryStream(Properties.Resources.ToggleTrue))
                {
                    BitmapImage image = new BitmapImage();
                    image.BeginInit();
                    image.CacheOption = BitmapCacheOption.OnLoad;
                    image.StreamSource = stream;
                    image.EndInit();
                    bitFon = image;
                }



                //Идет модификация некоторых картинок путем перезаписи некоторых байтов при некоторых условиях. Все картинки в ресурсах программы храняться в формате BGRA32
                WriteableBitmap writeable = new WriteableBitmap(bitFon);
                WriteableBitmap writeable2 = new WriteableBitmap(bitFon);

                int stride = writeable.PixelWidth * (writeable.Format.BitsPerPixel / 8);
                byte[] pixels = new byte[bitFon.PixelHeight * stride];
                byte[] pixels2 = new byte[bitFon.PixelHeight * stride];

                bitFon.CopyPixels(pixels, stride, 0);

                bitFon.CopyPixels(pixels2, stride, 0);

                for (int i = 0; i < pixels.Length - 4; i += 4)
                {
                    if (pixels[i + 3] != 0)
                    {
                        pixels[i + 2] = 255;
                        pixels[i + 1] = 0;
                        pixels[i + 0] = 0;
                    }

                    if (pixels2[i + 3] != 0)
                    {
                        pixels2[i + 2] = 0;
                        pixels2[i + 1] = 255;
                        pixels2[i + 0] = 0;
                    }
                }

                Int32Rect rect = new Int32Rect(0, 0, writeable.PixelWidth, writeable.PixelWidth);

                writeable.WritePixels(rect, pixels, stride, 0);

                redFon = writeable;


                writeable2.WritePixels(rect, pixels2, stride, 0);

                greenFon = writeable2;
            }

            public void ResetTo(int count)//Иземенение количиства анимаций и иконок. В это время отключаеться поток с анимациями.
            {
                updateList = true;
                if (points.Count > count && count != 0)
                {
                    int c = points.Count - count;
                    for (int i = 0; i < c; i++)
                    {
                        points[0].Remove();
                        points.Remove(points[0]).ToString();
                    }
                }
                else if (points.Count < count)
                {
                    int c = count - points.Count;
                    for (int i = 0; i < c; i++)
                    {
                        Point point = new Point(list, this);
                        points.Add(point);
                    }
                }
                else if (count == 0)
                {
                    for (int i = 0; i < points.Count; i++)
                    {
                        points[i].Remove();
                    }
                    points.Clear();
                }
                for (int i = 0; i < points.Count; i++)
                {
                    points[i].Reset();
                }
                updateList = false;
            }

            public void SetParams(bool[] bools)//Установка параметров для иконок из панели прохождения теста.
            {
                for (int i = 0; i < points.Count; i++)
                {
                    points[i].SetType(bools[i]);
                }
            }

            private async void AsyncUpdate()
            {
                Stopwatch timer = new Stopwatch();//Таймер для отсчета промежутка между кадрами
                timer.Start();
                while (true)//Бесконечный цыкл
                {
                    float time = timer.ElapsedMilliseconds;
                    timer.Restart();
                    if (!updateList)
                    {
                        for (int i = 0; i < points.Count; i++)
                        {
                            if (updateList)
                            {
                                break;
                            }
                            points[i].Update(time / 1000f);//Вызов обновление у иконок с анимациями с временем промежутка в секундах
                        }
                    }
                    await Task.Delay(10);//Минимальная задержка между кадрами 10 милисекунд
                }
            }

            //Класс для иконок с анимациями
            private class Point
            {
                StackPanel list;

                Grid grid;

                Image image;

                Image imageFonGreen;
                Image imageFonRed;

                PointRender render;

                bool type;

                float animRed = 0f;

                float animGreen = 0f;

                public Point(StackPanel list, PointRender point)//Инициализация
                {
                    render = point;

                    this.list = list;


                    grid = new Grid();
                    list.Children.Add(grid);
                    grid.Width = 20;
                    grid.Height = 20;


                    image = new Image();
                    grid.Children.Add(image);

                    image.Height = 20;
                    image.Width = 20;

                    imageFonGreen = new Image();
                    grid.Children.Add(imageFonGreen);

                    imageFonGreen.Height = 20;
                    imageFonGreen.Width = 20;

                    imageFonRed = new Image();
                    grid.Children.Add(imageFonRed);

                    imageFonRed.Height = 20;
                    imageFonRed.Width = 20;


                    image.Source = render.falseImage;

                    imageFonRed.Source = render.redFon;
                    imageFonGreen.Source = render.greenFon;
                    imageFonGreen.Opacity = 0;
                    imageFonRed.Opacity = 0;
                }

                public void Reset()//Обнуление
                {
                    image.Source = render.falseImage;
                    animGreen = 0f;
                    animRed = 0;
                    imageFonGreen.Opacity = 0;
                    imageFonRed.Opacity = 0;
                    type = false;
                }

                public void SetType(bool t)//Установка того чему равна иконка. И то какой цвет будет наложен поверх и анимирован.
                {
                    if (t != type)
                    {
                        type = t;
                        if (type)
                        {
                            image.Source = render.trueImage;
                            animGreen = 1.4f;
                            animRed = 0f;
                        }
                        else
                        {
                            image.Source = render.falseImage;
                            animGreen = 0f;
                            animRed = 1.4f;
                        }
                    }
                }

                public void Update(float time)//Вызываеться в другом потоке для анимации
                {
                    if (animGreen >= 0)
                    {
                        imageFonGreen.Opacity = Math.Clamp((double)(animGreen / 1.4f), 0, 1);
                        animGreen -= time;
                    }
                    if (animRed >= 0)
                    {
                        imageFonRed.Opacity = Math.Clamp((double)(animRed / 1.4f), 0, 1);
                        animRed -= time;
                    }
                }

                public void Remove()//Уничтожение иконки
                {
                    list.Children.Remove(grid);
                    grid = null;
                    image = null;
                }
            }
        }
    }
}
