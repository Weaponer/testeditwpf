﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using TextEditorWPF.Words;

namespace TextEditorWPF.Panels
{
    public class CreateTextPanel : Panel//Панель для создания текста.
    {

        private Button backMenu;

        private TextBlock promptText;

        private TextBox fieldInput;

        private Button ready;

        private Button save;


        bool stageTwo;

        string textStart;

        Word[] wordsTextStart;

        string saveTextTwoStage;


        public void SetElements(Button backToCreateMenu, Button ready, Button save, TextBox field, TextBlock prompt)
        {
            promptText = prompt;
            fieldInput = field;
            this.ready = ready;
            this.save = save;

            backMenu = backToCreateMenu;
            SetEvents();
            SetStageOne();
        }

        protected override void CallOpen()
        {
            stageTwo = false;
            textStart = "";
            wordsTextStart = null;
            fieldInput.Text = textStart;
            ready.Visibility = Visibility.Collapsed;
            save.Visibility = Visibility.Collapsed;
            SetStageOne();
        }

        private void SetEvents()
        {
            backMenu.Click += BackToCreateMenu;
            fieldInput.TextChanged += EditTextPanel;
            ready.Click += ReadyClick;
            save.Click += SaveText;
        }

        private void BackToCreateMenu(object sender, RoutedEventArgs e)
        {

            Back();
        }

        private void EditTextPanel(object sender, RoutedEventArgs e)
        {
            EditText();
        }

        private void ReadyClick(object sende, RoutedEventArgs e)
        {
            Ready();
        }

        private void SaveText(object sende, RoutedEventArgs e)
        {
            Save();
        }

        private void Save()
        {
            if (stageTwo)
            {
                TextFile textFile = new TextFile();
                textFile.WriteWords(wordsTextStart, TextInformation.GetArrayWordsFromText(fieldInput.Text));
                Files.FileSystem.SaveTextFile(textFile);
            }
        }

        private void EditText()//Пользователь изменил панель
        {
            if (!stageTwo)//Состояние первое
            {
                Word[] words = TextInformation.GetArrayWordsFromText(fieldInput.Text);
                if (words.Length > 0)
                {
                    ready.Visibility = Visibility.Visible;
                }
                else
                {
                    ready.Visibility = Visibility.Collapsed;
                }
            }
            else//Состояние второе
            {
                Word[] newWords = TextInformation.GetArrayWordsFromText(fieldInput.Text);
                if (newWords.Length != wordsTextStart.Length)
                {
                    fieldInput.Text = saveTextTwoStage;
                    save.Visibility = Visibility.Collapsed;
                }
                else
                {
                    bool a = true;
                    for (int i = 0; i < newWords.Length; i++)
                    {
                        if (newWords[i].Letters != wordsTextStart[i].Letters)
                        {
                            a = false;
                        }
                    }
                    if (a)
                    {
                        save.Visibility = Visibility.Collapsed;
                    }
                    else
                    {
                        save.Visibility = Visibility.Visible;
                    }
                }
                saveTextTwoStage = new string(fieldInput.Text);
            }
        }

        private void Ready() //Нажата кнопка Готово. Переход на новый этап создания текста
        {
            SetStageTwo();
        }

        private void Back() //Кнопка Назад. Если создание на втором этапе - тогда возвращение на 1 этап, или возвращение в меню.
        {
            if (stageTwo)
            {
                SetStageOne();
            }
            else
            {
                PanelControll.OpenPanel(PanelControll.MenuCreateTest);
            }
        }

        private void SetStageOne()//Переход в состояние 1
        {
            promptText.Text = "Введите текст";
            stageTwo = false;
            fieldInput.Text = textStart;
            save.Visibility = Visibility.Collapsed;
            ready.Visibility = Visibility.Visible;
        }

        private void SetStageTwo()//Переход в состояние 2
        {
            promptText.Text = "Внесите изменения";
            stageTwo = true;
            ready.Visibility = Visibility.Collapsed;
            textStart = new string(fieldInput.Text);
            saveTextTwoStage = new string(fieldInput.Text);
            wordsTextStart = TextInformation.GetArrayWordsFromText(textStart);
        }
    }
}
