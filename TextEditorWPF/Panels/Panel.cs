﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace TextEditorWPF.Panels
{
    public static class PanelControll //Статичиский класс который отвечает за хранение и открытие панелей.
    {
        public static BaseMenuPanel BaseMenu { get; private set; }

        public static MenuCreateTestPanel MenuCreateTest { get; private set; }

        public static CreateTextPanel CreateText { get; private set; }

        public static EditTextPanel EditText { get; private set; }

        public static CreateTestPanel CreateTest { get; private set; }

        public static PassTestPanel PassTest { get; private set; }

        public static OptionPanel Option { get; private set; }

        private static Panel oldPanel;

        public static void SetPanels(BaseMenuPanel baseMenuPanel, MenuCreateTestPanel menuCreate, CreateTextPanel createTextPanel,
            EditTextPanel editTextPanel, CreateTestPanel createTest, PassTestPanel passTest
            )
        {
            BaseMenu = baseMenuPanel;
            MenuCreateTest = menuCreate;
            CreateText = createTextPanel;
            EditText = editTextPanel;
            CreateTest = createTest;
            PassTest = passTest;
        }

        public static void SetOption(OptionPanel option)
        {
            Option = option;
        }

        public static void OpenPanel(Panel panel)
        {
            if (oldPanel != null)
                oldPanel.ClosePanel();

            panel.OpenPanel();
            oldPanel = panel;
        }
    }

    public abstract class Panel// Базовый класс для всех панелей главного окна.
    {
        public bool IsOpenPanel { get; private set; }

        Grid grid;

        public void SetGrid(Grid grid)//Установка основы из интерфейса для панели
        {
            this.grid = grid;
        }

        public void OpenPanel()//Открытие панели. Может быть вызван только PanelControll
        {
            IsOpenPanel = true;

            CallOpen();
            grid.Visibility = Visibility.Visible;
        }

        protected virtual void CallOpen()//Вызов метода перед открытием панели. Этот метод может быть переопределен классом унаследованным от это класса.
        {

        }

        public void ClosePanel()//Тоже самое что и открытие, только закрытие.
        {
            IsOpenPanel = false;

            CallClose();
            grid.Visibility = Visibility.Collapsed;
        }

        protected virtual void CallClose()//Тоже самое что и переопределение открытия только закрытие.
        {

        }
    }


}
