﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;

namespace TextEditorWPF.Panels
{
    public class CreateTestPanel : Panel//Панель для создания теста.
    {

        private Button backMenu;

        private TextBlock setNamesPanels;

        private Button save;

        private Button load;

        TextFile[] texts;

        public void SetElement(Button back, Button save, Button load, TextBlock textNames)//При создания каждой панели есть подобный метод который отправляет панели список елементов ие интерфейса.
        {
            backMenu = back;
            setNamesPanels = textNames;
            this.save = save;
            this.load = load;
            SetEvents();
        }

        private void SetEvents()//Подключает события полученных елементов к внутренним методам.
        {
            backMenu.Click += BackToCreateTestMenu;
            save.Click += SaveFile;
            load.Click += LoadFiles;
        }

        private void SaveFile(object sende, RoutedEventArgs e)//Вызов сохранения файла.
        {
            Save();
        }

        private void LoadFiles(object sende, RoutedEventArgs e)//Вызов загрузки файла.
        {
            Load();
        }

        private void BackToCreateTestMenu(object sende, RoutedEventArgs e)//Вызов возвращение на предыдущию панель.
        {
            PanelControll.OpenPanel(PanelControll.MenuCreateTest);
        }

        private void Save()//Сохранение нового теста
        {
            TestFile test = new TestFile();
            test.SetTexts(texts);
            Files.FileSystem.SaveTestFile(test);
        }

        private void Load()//Загрузка текстов
        {
            SetTexts(Files.FileSystem.LoadTextFiles());
        }

        private void SetTexts(TextFile[] textFiles)//Установка списка текстов в панель.
        {
            texts = textFiles;
            setNamesPanels.Text = "";

            if (textFiles != null)
            {
                for (int i = 0; i < textFiles.Length; i++)
                {
                    setNamesPanels.Text += texts[i].Name;
                    setNamesPanels.Text += "\n";
                }
            }
        }
    }
}
