﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;

namespace TextEditorWPF.Panels
{
    public class MenuCreateTestPanel : Panel//Панель для создания теста.
    {
        private Button createText;
        private Button editText;
        private Button createTest;
        private Button backPanel;

        public void SetButtons(Button createText, Button editText, Button createTest, Button backPanel)
        {
            this.createTest = createTest;
            this.createText = createText;
            this.editText = editText;
            this.backPanel = backPanel;
            SetEvents();
        }

        private void SetEvents()
        {
            backPanel.Click += BackPanel;
            createText.Click += CreateText;
            editText.Click += EditText;
            createTest.Click += CreateTest;
        }

        private void BackPanel(object sender, RoutedEventArgs e)
        {
            PanelControll.OpenPanel(PanelControll.BaseMenu);
        }

        private void CreateText(object sender, RoutedEventArgs e)
        {
            PanelControll.OpenPanel(PanelControll.CreateText);
        }

        private void EditText(object sender, RoutedEventArgs e)
        {
            PanelControll.OpenPanel(PanelControll.EditText);
        }

        private void CreateTest(object sender, RoutedEventArgs e)
        {
            PanelControll.OpenPanel(PanelControll.CreateTest);
        }
    }
}
