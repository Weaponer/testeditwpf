﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TextEditorWPF.Words //Классы для работы с текстом и изменения.
{
    [Serializable]
    public class Word//Класс слова который содержит символы и отступ в конеце.
    {
        public string Letters;
        public string Gap;
    }

    public static class TextInformation// Статичиский класс который нужен для обработки и трансформации текста с разных состояний.
    {
        public static Word[] GetArrayWordsFromText(string text)//Разделение текста на слова и запись в массив.
        {
            string[] words = text.Split(new char[] { ' ', '\n', '\r'}, StringSplitOptions.RemoveEmptyEntries) ;
            Word[] w = new Word[words.Length];

            if (w.Length > 0)
            {
                int numWord = 0;
                for (int i = 0; i < text.Length; i++)
                {
                    if ((text[i] != ' ' && text[i] != '\n' && text[i] != '\r') && text.IndexOf(words[numWord], i) != -1)
                    {
                        w[numWord] = new Word();
                        w[numWord].Gap = new string("");
                        w[numWord].Letters = words[numWord];
                        i += words[numWord].Length - 1;
                        numWord++;
                        if (numWord == w.Length)
                        {
                            break;
                        }
                    }
                    if (numWord > 0 && (text[i] == ' ' || text[i] == '\n' || text[i] == '\r'))
                    {
                        w[numWord - 1].Gap += text[i];
                    }
                }
            }

            return w;
        }

        public static string GetText(Word[] words)//Получения текста из массива слов.
        {
            string text = new string("");
            for (int i = 0; i < words.Length; i++)
            {
                text += words[i].Letters;
                if (i < words.Length - 1)
                    text += words[i].Gap;
            }
            return text;
        }

        public static void TabWords(Word[] words)// Применения специальных команды которые вписаны в слова (/- ... / и /_ ... /) и пересохранения в полученый массив.
        {
            for (int i = 0; i < words.Length; i++)
            {
                if (words[i].Letters.Length > 3)
                {

                    bool isOne = false;
                    bool isTwo = false;
                    int index = 0;
                    for (int i2 = 0; i2 < words[i].Letters.Length; i2++)
                    {
                        if (words[i].Letters.Length - i > 1)
                        {
                            if (!isOne && !isTwo && (words[i].Letters[i2] == '/' ||  words[i].Letters[i2] == '\\')&& words[i].Letters[i2 + 1] == '-')
                            {
                                isOne = true;
                                index = i2;
                            }
                            else if (!isTwo && !isOne && (words[i].Letters[i2] == '/' || words[i].Letters[i2] == '\\') && words[i].Letters[i2 + 1] == '_')
                            {
                                isTwo = true;
                                index = i2;
                            }
                            else if ((isOne || isTwo) && (words[i].Letters[i2] == '/' || words[i].Letters[i2] == '\\'))
                            {
                                if (isOne)
                                {
                                    int count = i2 - index + 1 - 3;
                                    for (int i3 = index; i3 <= i2; i3++)
                                    {
                                        words[i].Letters = words[i].Letters.Remove(index, 1);
                                    }
                                    for (int i3 = 0; i3 < count; i3++)
                                    {
                                        words[i].Letters = words[i].Letters.Insert(index, "_");
                                    }
                                }
                                else
                                {
                                    words[i].Letters = words[i].Letters.Remove(index, i2 - index + 1);

                                    words[i].Letters = words[i].Letters.Insert(index, "_");
                                }
                                isOne = false;
                                isTwo = false;
                                i2 -= (i2 - index + 1);
                            }
                        }
                    }
                }
            }
        }
    }
}
